#include <stdlib.h>
#include <malloc.h>
#include <jni.h>
#include "MergeSort.h"

void merge (int *a, int n, int m) {
    int i, j, k;
    int *x = malloc(n * sizeof (int));

    for (i = 0, j = m, k = 0; k < n; k++) {
        x[k] = j == n      ? a[i++]
             : i == m      ? a[j++]
             : a[j] < a[i] ? a[j++]
             :               a[i++];
    }

    for (i = 0; i < n; i++) {
        a[i] = x[i];
    }

    free(x);
}

void sort(int *a, int n) {
    if (n < 2) return;
    int m = n / 2;
    sort(a, m);
    sort(a + m, n - m);
    merge(a, n, m);
}

JNIEXPORT void JNICALL Java_zone_haze_alevel_homework_hw04_MergeSort_sort(JNIEnv *env, jclass clazz, jintArray arr) {
    jint *body = (*env)->GetIntArrayElements(env, arr, 0);
    int len = (*env)->GetArrayLength(env, arr);
    sort(body, len);

    (*env)->SetIntArrayRegion(env, arr, 0, len, body);
}