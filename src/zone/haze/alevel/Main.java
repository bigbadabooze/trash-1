package zone.haze.alevel;

import zone.haze.alevel.homework.hw04.MergeSort;
import zone.haze.alevel.homework.hw04.QuickSort;
import zone.haze.util.Timer;
import zone.haze.util.file.Files;

import java.util.Arrays;

public class Main {
    private static final Files files = Files.instance;

    public static void main(String... args) {
        System.out.println("Hello world!");

        System.out.println(files.getDataFile("global", "restaurant03", "db.txt").exists());
        System.out.println(files.getDataFile("global", "restaurant03", "db.txt").canRead());
    }

    private static void homework04() {
        int[] array1 = Arrays.copyOf(MergeSort.testArray, MergeSort.testArray.length);
        int[] array2 = Arrays.copyOf(MergeSort.testArray, MergeSort.testArray.length);
        int[] array3 = Arrays.copyOf(MergeSort.testArray, MergeSort.testArray.length);

        new Timer("My merge sort");
        MergeSort.sort(array1);
        Timer.stop();

        new Timer("My quick sort");
        QuickSort.sort(array2);
        Timer.stop();

        new Timer("Default arrays sort");
        Arrays.sort(array3);
        Timer.stop();

        System.out.println(Arrays.equals(array1, array2) && Arrays.equals(array2, array3) ? "All is good sorted." : "There is some problems.");
        Timer.printAll();
    }

}
