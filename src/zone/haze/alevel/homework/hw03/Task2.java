package zone.haze.alevel.homework.hw03;

public class Task2 {
    private int[][] square;

    Task2(int[][] square) {
        this.square = square;
    }

    public boolean isMagicSquare() {
        if (square.length == 0 || square[0].length == 0) {
            return false;
        }

        for (int i = square.length; i-- > 0; ) {
            if (square.length != square[i].length) return false;
        }

        int dimensions = square.length;
        long check = dimensions * (dimensions * dimensions + 1) / 2;

        long diag1 = 0, diag2 = 0;

        for (int i = 0, row = 0, col = 0; i < dimensions; i++, row = 0, col = 0) {
            diag1 += square[i][i];
            diag2 += square[(dimensions - 1) - i][(dimensions - 1) - i];

            for (int j = 0; j < dimensions; j++) {
                row += square[i][j];
                col += square[j][i];
            }

            if (check != row || row != col) {
                return false;
            }
        }

        return check == diag1 && diag1 == diag2;
    }

    @Override
    public String toString() {
        return isMagicSquare() ? "magic" : "not magic";
    }

    public static void main(String... args) {
        Task2 success = new Task2(new int[][]{
                {6, 1, 8},
                {7, 5, 3},
                {2, 9, 4},
        });

        Task2 failure = new Task2(new int[][]{
                {1, 2, 13, 15},
                {9, 3, 14, 8},
                {7, 12, 5, 10},
                {11, 6, 16, 4},
        });

        System.out.printf("First square is %s!\n", success);
        System.out.printf("Second square is %s!\n", failure);
    }

}
