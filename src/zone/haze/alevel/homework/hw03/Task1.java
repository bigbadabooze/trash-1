package zone.haze.alevel.homework.hw03;

import java.util.Scanner;

public class Task1 {
    private static Scanner reader = new Scanner(System.in);

    public void pascalTriangle(int height) {
        long num;
        int row;
        height = height - 1;

        for (int i = 0; i <= height; i++) {
            num = 1;
            row = i + 1;

            System.out.print(" ".repeat(height - i));

            for (int col = 0; col <= i; col++) {

                if (col > 0) {
                    num = num * (row - col) / col;
                }

                System.out.print(num + " ");
            }

            System.out.println();
        }
    }

    public static void main(String... args) {
        Task1 task = new Task1();
        task.pascalTriangle(reader.nextInt());
    }

}
