package zone.haze.alevel.homework.hw06;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework06 {
    private static Scanner reader = new Scanner(System.in);
    private static Random random = new Random();

    // 1
    public static String randomString(String input) {
        StringBuilder result = new StringBuilder();
        char[] origin = noWhitespaces(input).toCharArray();
        int len = origin.length;

        int words = random.nextInt(20);

        for (int i = 0; i < words; i++) {
            int chars = random.nextInt(10);

            for (int j = 0; j < chars; j++) {
                result.append(origin[random.nextInt(len)]);
            }

            result.append(' ');
        }

        return clearString(result.toString());
    }

    // 2
    public static boolean isIdentityString(String input) {
        return input.matches("^[\\w_][\\w\\d_]+$");
    }

    // 3
    public static boolean isPalindrome(String input) {
        StringBuilder result = new StringBuilder(noWhitespaces(input));
        int half = result.length() / 2;

        return result.substring(0, half).equals(result.reverse().substring(0, half));
    }

    // 4
    public static ArrayList<Integer> filterNumbes(String input) {
        Pattern nums = Pattern.compile("-?\\d+");
        Matcher match = nums.matcher(input);
        ArrayList<Integer> result = new ArrayList<>();

        while (match.find()) {
            result.add(Integer.parseInt(match.group()));
        }

        return result;
    }

    // 5
    public static String distinct(String input) {
        HashSet<Character> result = new HashSet<>();
        char[] search = noWhitespaces(input).toCharArray();
        StringBuilder out = new StringBuilder();

        for (char character : search) result.add(character);
        for (char character : result) out.append(character);

        return out.toString();
    }

    // 6
    public static String replacement() {
        String string, substring, replacement;

        System.out.print("Main string: ");
        string = reader.nextLine();

        System.out.print("String that need to be replaced: ");
        substring = reader.nextLine();

        System.out.print("Replacement: ");
        replacement = reader.nextLine();

        return string.replace(substring, replacement);
    }

    // 7
    public static void theLongestString() {
        String input;
        ArrayList<String> strings = new ArrayList<>();
        int max = -1;

        do {
            System.out.print("Введите строку: ");
            input = reader.nextLine();

            if (max < input.length()) {
                max = input.length();
            }

            strings.add(input);
        } while (!"exit".equals(input));

        strings.remove(strings.size() - 1);

        for (int i = 0; i < strings.size(); i++) {
            System.out.print(strings.get(i).length() == max ? String.format("%d\n: %s", i, strings.get(i)) : "");
        }
    }

    // 8
    public static String theLongestWord(String input) {
        String[] strings = clearString(input).split(" ");

        Arrays.sort(strings, Comparator.comparingInt(String::length).reversed());

        return strings[0];
    }

    // 9
    public static String clearString(String input) {
        return input.trim().replaceAll(" +", " ");
    }

    public static class Pair<A, B> {
        public A one;
        public B two;

        public Pair(A one, B two) {
            this.one = one;
            this.two = two;
        }
    }

    // 10
    public static Pair<Integer, Integer> amountOfChars(String input) {
        input = input.replaceAll("\\W", "").replaceAll("\\d", "");
        Pair<Integer, Integer> result = new Pair<>(0, 0) {
            @Override
            public String toString() {
                return String.format("Big: %d\nSmall: %d\n", one, two);
            }
        };

        for (char c : input.toCharArray()) {
            if (c > 'A' && c < 'Z') result.one++;
            if (c > 'a' && c < 'z') result.two++;
        }

        return result;
    }

    // 11
    public static int wordsAmount(String input) {
        return clearString(input).split(" ").length;
    }

    public static String noWhitespaces(String input) {
        return input.replaceAll("\\s", "");
    }

    public static void main(String... args) {

    }

}
