package zone.haze.alevel.homework.hw04;

import zone.haze.util.Timer;
import zone.haze.util.file.Paths;

import java.util.Arrays;
import java.util.Random;

public class MergeSort {
    public final static int[] testArray = new Random().ints(1, 9999999).limit(3000000).toArray();
    private static final Paths paths = Paths.instance;

    static {
        System.load(paths.libsString("homework", "libMergeSort.so"));
    }

    public static native void sort(int[] array);

    public static void main(String... args) {
        int[] array1 = Arrays.copyOf(testArray, testArray.length);
        int[] array2 = Arrays.copyOf(testArray, testArray.length);

        new Timer("Моя реализация merge sort");
        sort(array1);
        Timer.stop();

        new Timer("Стандартная реализация сортировки (Arrays)");
        Arrays.sort(array2);
        Timer.stop();

        System.out.println(Arrays.equals(array1, array2) ? "Массивы совпадают." : "Массивы не совпадают.");
        Timer.printAll();
    }

}
