package zone.haze.alevel.homework.hw12.person;

public class Person {

    private String name;
    private boolean gender;
    private int age;

    public Person() {
        name = "John Doe";
        gender = true;
        age = -1;
    }

    public Person(String name, String gender, int age) {
        this.name = name;
        this.gender = gender.equals("M");
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;

        return this;
    }

    public String getGender() {
        return gender ? "M" : "F";
    }

    public Person setGender(String gender) {
        this.gender = gender.equals("M");

        return this;
    }

    public int getAge() {
        return age;
    }

    public Person setAge(int age) {
        this.age = age;

        return this;
    }

    @Override
    public String toString() {
        return String.format(
                "%s, age: %d, gender: %s",
                getName(),
                getAge(),
                getGender()
        );
    }

}
