package zone.haze.alevel.homework.hw12.person;

public class CollegeStudent extends Student {

    private int year;
    private String major;

    public CollegeStudent() {
        year = (int) (Math.random() * 6);
        major = "Ninja";
    }

    public CollegeStudent(int year, String major) {
        this.year = year;
        this.major = major;
    }

    public CollegeStudent(String idNumber, double gpa, int year, String major) {
        super(idNumber, gpa);
        this.year = year;
        this.major = major;
    }

    public CollegeStudent(String name, String gender, int age, String idNumber, double gpa, int year, String major) {
        super(name, gender, age, idNumber, gpa);
        this.year = year;
        this.major = major;
    }

    public int getYear() {
        return year;
    }

    public CollegeStudent setYear(int year) {
        this.year = year;

        return this;
    }

    public String getMajor() {
        return major;
    }

    public CollegeStudent setMajor(String major) {
        this.major = major;

        return this;
    }

    @Override
    public String toString() {
        return String.format(
                "%s, year: %d, major: %s",
                super.toString(),
                getYear(),
                getMajor()
        );
    }

}
