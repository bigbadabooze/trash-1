package zone.haze.alevel.homework.hw12.person;

import java.util.Locale;

public class Teacher extends Person {

    private double salary;
    private String subject;

    public Teacher() {
        salary = 0.1;
        subject = "Unknown";
    }

    public Teacher(double salary, String subject) {
        this.salary = salary;
        this.subject = subject;
    }

    public Teacher(String name, String gender, int age, double salary, String subject) {
        super(name, gender, age);
        this.salary = salary;
        this.subject = subject;
    }

    public double getSalary() {
        return salary;
    }

    public Teacher setSalary(double salary) {
        this.salary = salary;

        return this;
    }

    public String getSubject() {
        return subject;
    }

    public Teacher setSubject(String subject) {
        this.subject = subject;

        return this;
    }

    @Override
    public String toString() {
        return String.format(
                Locale.ROOT,
                "%s, subject: %s, salary: %.2f",
                super.toString(),
                getSubject(),
                getSalary()
        );
    }

}
