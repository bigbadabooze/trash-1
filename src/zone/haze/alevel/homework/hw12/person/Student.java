package zone.haze.alevel.homework.hw12.person;

import java.util.Locale;

public class Student extends Person {
    private String idNumber;
    private double gpa;

    public Student() {
        idNumber = String.format("HS%05d", (int) (Math.random() * 65526));
        gpa = Math.random() * 5.01;
    }

    public Student(String idNumber, double gpa) {
        this.idNumber = idNumber;
        this.gpa = gpa;
    }

    public Student(String name, String gender, int age, String idNumber, double gpa) {
        super(name, gender, age);
        this.idNumber = idNumber;
        this.gpa = gpa;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public Student setIdNumber(String idNumber) {
        this.idNumber = idNumber;

        return this;
    }

    public double getGpa() {
        return gpa;
    }

    public Student setGpa(double gpa) {
        this.gpa = gpa;

        return this;
    }

    @Override
    public String toString() {
        return String.format(
                Locale.ROOT,
                "%s, student id: %s, gpa: %.1f",
                super.toString(),
                getIdNumber(),
                getGpa()
        );
    }

}
