package zone.haze.alevel.homework.hw12;

import zone.haze.alevel.homework.hw12.person.CollegeStudent;
import zone.haze.alevel.homework.hw12.person.Person;
import zone.haze.alevel.homework.hw12.person.Student;
import zone.haze.alevel.homework.hw12.person.Teacher;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Arrays.stream(new Person[]{
                new Person("Coach Bob", "M", 27),
                new Student("Lynne Brooke", "F", 16, "HS95129", 3.1),
                new Teacher("Duke Java", "M", 34, 50000.0, "Computer Science"),
                new CollegeStudent("Ima Frost", "F", 18, "UCB123", 4.0, 1, "English")
        }).forEach(System.out::println);
    }

}
