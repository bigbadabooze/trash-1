package zone.haze.util.file;

import java.nio.file.Path;

public enum Paths {
    instance;

    private final Path app = java.nio.file.Paths.get(Paths.class.getProtectionDomain().getCodeSource().getLocation().toString()).getParent();
    private final String separator = System.getProperty("file.separator");

    public Path combine(String part, String... parts) {
        return java.nio.file.Paths.get(part, parts);
    }

    public Path baseDir() {
        return app;
    }

    public String getSeparator() {
        return separator;
    }

    private Path moduleDirCombiner(String moduleDir, String... parts) {
        return combine(combine(baseDir().toString(), moduleDir).toString(), parts);
    }

    private String moduleDirString(boolean clear, Path module) {
        if (!clear)
            return module.toString();

        return module.toString().replaceAll("(file:|jar:|dir:|directory:|uri:|url:)", "");
    }

    public Path libs(String... parts) {
        return moduleDirCombiner("libs", parts);
    }

    public String libsString(boolean clear, String... parts) {
        return moduleDirString(clear, libs(parts));
    }

    public String libsString(String... parts) {
        return libsString(true, parts);
    }

    public Path data(String... parts) {
        return moduleDirCombiner("data", parts);
    }

    public String dataString(boolean clear, String... parts) {
        return moduleDirString(clear, data(parts));
    }

    public String dataString(String... parts) {
        return libsString(true, parts);
    }

}
